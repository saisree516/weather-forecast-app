import * as React from 'react';
import Box from '@mui/material/Box';
import { Grid } from '@mui/material';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import CardMedia from '@mui/material/CardMedia';
import { styled } from '@mui/material/styles';
import axios from 'axios';

import { useState } from 'react';
import {
  LineChart,
  ResponsiveContainer,
  Legend, Tooltip,
  Line,
  XAxis,
  YAxis,
  CartesianGrid
} from 'recharts';

export default function TableData() {

  const chartData = [
    {
      temparature: '60',
      timings: '0:00 am',

    },
    {
      temparature: '65',
      timings: '3:00 am',
    },
    {
      temparature: '70',
      timings: '6:00 am',
    },
    {
      temparature: '75',
      timings: '9:00 am',
      fees: 5
    },
    {
      temparature: '80',
      timings: '12:00 pm',
      fees: 4
    },
    {
      temparature: '85',
      timings: '3:00 pm',
      fees: 8
    },
    {
      temparature: '90',
      timings: '6:00 pm',
    },
  ];

  const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }));


  const [alignment, setAlignment] = React.useState('web');
  const [totalData, setTotalData] = useState([])
  const [table, setTableData] = useState(false);
  const [chart, setTable] = useState(true);
  const [count, setCount] = useState(0);

  const linechart = () => {
    setTable(true);
    setTableData(false);
  }

  const datatable = () => {
    setTableData(true);
    setTable(false);
  }

  axios.get('https://api.weatherapi.com/v1/forecast.json?key=0b81c00069be4d3fa7262132220408&q=hyderabad&days=7&aqi=no&alerts=no', {
  })
    .then(res => {
      console.log([res.data])
      setTotalData([res.data])

    })
    .catch(err => {
      console.log(err)
    })

  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };
  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', marginTop: "20px" }}>
      <Box sx={{ display: 'flex', flexDirection: 'row' }}>
        <Box sx={{ width: '100%', marginLeft: "50px" }}>
          <Grid container rowSpacing={0} sx={{ marginTop: "10px" }} >
            <Grid item xs={1} sx={{ border: "Solid", borderColor: "gray", borderRadiusLeft: "5px" }}>
              <Item sx={{ bgcolor: "#BFD7ED" }}>Today</Item>
            </Grid>
            <Grid item xs={1} style={{margin:5}}>
              <Item>Monday</Item>
            </Grid>
            <Grid item xs={1} style={{margin:5}}>
              <Item>Tuesday</Item>
            </Grid>
            <Grid item xs={1} style={{margin:5}}>
              <Item>Wednesday</Item>
            </Grid>
            <Grid item xs={1} style={{margin:5}}>
              <Item>Thursday</Item>
            </Grid>
            <Grid item xs={1} style={{margin:5}}>
              <Item>Friday</Item>
            </Grid>
            <Grid item xs={1} style={{margin:5}}>
              <Item>Saturday</Item>
            </Grid>
          </Grid>
        </Box>
        <Grid sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'end' }}>
          <ToggleButtonGroup
            color="primary"
            value={alignment}
            exclusive
            onChange={handleChange}
          >
            <ToggleButton value="web" onClick={datatable}>chart</ToggleButton>
            <ToggleButton value="android" onClick={linechart}>data</ToggleButton>
          </ToggleButtonGroup></Grid>
      </Box>
      {table ?
        <Box sx={{ marginTop: "20px", marginLeft: "30px" }}>
          <ResponsiveContainer width="100%" aspect={3}>
            <LineChart data={chartData} margin={{ right: 300 }}>
              <CartesianGrid />
              <YAxis dataKey="temparature"
                interval={'preserveStartEnd'} ></YAxis>
              <XAxis dataKey="timings"
                interval={'preserveStartEnd'} />

              <Legend />
              <Tooltip />
              <Line dataKey="temparature"
                stroke="black" activeDot={{ r: 8 }} />
            </LineChart>
          </ResponsiveContainer>
        </Box> : ""}
      {chart ?
        <Box sx={{ marginLeft: "30px", marginTop: "20px" }}>
          <TableContainer sx={{ marginLeft: "30px" }} component={Paper}>
            <Table sx={{ minWidth: 150 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Sunday</TableCell>
                  <TableCell>Icon</TableCell>
                  <TableCell>Temp</TableCell>
                  <TableCell>Wind</TableCell>
                  <TableCell>Precip</TableCell>
                  <TableCell>Cloud</TableCell>
                  <TableCell>Humidity</TableCell>
                  <TableCell>Pressure</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {totalData && totalData.map((row) => (
                  <TableRow

                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                    </TableCell>
                    <TableCell align="right"><CardMedia sx={{ height: 20, width: 20 }}
                      component="img"
                      image="http://cdn.weatherapi.com/weather/64x64/day/119.png"
                    /></TableCell>
                    <TableCell align="right" style={{textAlign:"left"}}>{row.forecast.forecastday[count].day.avgtemp_c}</TableCell>
                    <TableCell align="right" style={{textAlign:"left"}}>{row.current.wind_mph}</TableCell>
                    <TableCell align="right" style={{textAlign:"left"}}>{row.current.precip_mm}</TableCell>
                    <TableCell align="right" style={{textAlign:"left"}}>{row.current.cloud}</TableCell>
                    <TableCell align="right" style={{textAlign:"left"}}>{row.current.humidity}</TableCell>
                    <TableCell align="right" style={{textAlign:"left"}}>{row.current.pressure_mb}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box> : ""}
    </Box>

  );
}
