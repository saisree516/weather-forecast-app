import * as React from 'react';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';

export default function ColorToggleButton(user,setUser) {
  const [alignment, setAlignment] = React.useState('web');
  const[f,setF]=React.useState(true);
  const[c,setC]=React.useState(false);

  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  const onChangeF=()=>{
    setF(true);
    setC(false);
    setUser(true)
  }

  const onChangeC=()=>{
    setF(false);
    setC(true);
    setUser(true)
  }

  return (
    <ToggleButtonGroup
      color="primary"
      value={alignment}
      exclusive
      onChange={handleChange}
    >
      <ToggleButton value="web" onClick={onChangeF}>°F</ToggleButton>
      <ToggleButton value="android" onClick={ onChangeC }>°C</ToggleButton>
    </ToggleButtonGroup>
  );
}
