import * as React from 'react';
import FormControl from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';


export default function Search() {
  return (
    <Box sx={{ width: '100%',marginTop:"20px",marginLeft:"30px",display:'flex',flexDirection:'row',justifyContent:'center'}}>
      <Grid xs container direction="row" spacing={2} rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={10}>
        <FormControl sx={{ width: '130ch'}}>
        <OutlinedInput placeholder="Please enter cityname,US Zip Code,Canada Postal Code,UK PostCode,ip,metar,etc" />        
      </FormControl>
        </Grid>
        </Grid>
    </Box>
  );
}
