import * as React from 'react';
import { useEffect } from 'react';
import Box from '@mui/material/Box';
import { Grid } from '@mui/material';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import { useState } from 'react';
import CardMedia from '@mui/material/CardMedia';
import axios from 'axios';

export default function Card() {
  const [alignment, setAlignment] = React.useState('web');
  const [location, setLocation] = useState('')
  const [table, setTableData] = useState(false);
  const [chart, setTable] = useState(true);

  const linechart = () => {
    setTable(true);
    setTableData(false);
  }

  const datatable = () => {
    setTableData(true);
    setTable(false);
  }
  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  useEffect(() => {
    axios.get("https://api.weatherapi.com/v1/forecast.json?key=0b81c00069be4d3fa7262132220408&q=hyderabad&days=7&aqi=no&alerts=no").then(response => {
      setLocation(response.data.location)
    })
  }, [])

  return (
    <>
      <Box sx={{ display: 'flex', flexDirection: 'column' }}>
        <Box sx={{ width: '100%', display: 'flex', marginLeft: "30px", flexDirection: 'row', justifyContent: 'center' }}>
          <Grid xs container direction="row" spacing={2} rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
            <Grid item xs={4}>
              <h2>{location.name} Weather Forecast</h2>
            </Grid>
            <Grid item xs={4}>
              <h3 style={{color:"grey",marginRight:8}}>{location.region},{location.country}</h3>
            </Grid>
            <Grid item xs={4} sx={{ marginTop: "20px", display: 'flex', flexDirection: 'row', justifyContent: 'end' }} >
              <Grid sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'end' }}>
                <ToggleButtonGroup
                  color="primary"
                  value={alignment}
                  exclusive
                  onChange={handleChange}
                >
                  <ToggleButton value="web" onClick={datatable}>°C</ToggleButton>
                  <ToggleButton value="android" onClick={linechart}>°F</ToggleButton>
                </ToggleButtonGroup></Grid>
            </Grid>
          </Grid>
        </Box>
        {table ?
          <Box sx={{ display: 'flex', flexDirection: 'column', marginLeft: "60px", width: 1100, height: 350, backgroundColor: '#190D87', borderRadius: "20px" }}>
            <Grid xs container direction="row" spacing={2} rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
              <CardMedia sx={{ height: 40, width: 70, marginLeft: "50px", marginTop: "40px" }}
                component="img"
                image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
              />
              <Grid item xs={5} sx={{ marginLeft: '20px' }}>

                <h1 style={{ color: "#E2E0DE" }}>25.8°C Sunday</h1>
                <p style={{ color: "#E2E0DE" }}>Light Rain shower</p>
              </Grid>
              <Grid xs container direction="row" style={{ marginLeft: "40px" }}>
                <Grid xs container columnSpacing={{ xs: 1 }}><p style={{ width: "40px", color: "#E2E0DE" }}>Wind 18.0 Kmph</p></Grid>
                <Grid xs container columnSpacing={{ xs: 1 }}><p style={{ width: "40px", color: "#E2E0DE" }}>Precip 0.00mm</p></Grid>
                <Grid xs container columnSpacing={{ xs: 1 }}><p style={{ width: "40px", color: "#E2E0DE" }}>Pressure 1007.0 mb</p></Grid>
              </Grid>
            </Grid>
            <Grid xs container direction="row" style={{ marginLeft: "30px" }} spacing={2} rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
                />
                <p style={{ color: "#E2E0DE" }}>Monday</p>
                  <p style={{ color: "#E2E0DE" }}>
                  28.8 °C</p>
                  <p style={{ color: "#E2E0DE" }}>
                  Partly Cloudy</p>
              </Grid>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/119.png"
                />
                <p style={{ color: "#E2E0DE" }}>Tuesday</p>
                <p style={{ color: "#E2E0DE" }}>29.6 °C</p>
                <p style={{ color: "#E2E0DE" }}>Cloudy</p>
              </Grid>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 50, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
                />
                <p style={{ color: "#E2E0DE" }}>Wednesday</p>
                <p style={{ color: "#E2E0DE" }}>29.5 °C</p>
                <p style={{ color: "#E2E0DE" }}>Sunshine </p>
              </Grid>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/119.png"
                />
                <p style={{ color: "#E2E0DE" }}>Thursday</p>
                <p style={{ color: "#E2E0DE" }}>24.9 °C</p>
                <p style={{ color: "#E2E0DE" }}>Rainy</p>
              </Grid>

              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/119.png"
                />
                <p style={{ color: "#E2E0DE" }}>Friday</p>
                <p style={{ color: "#E2E0DE" }}>26.1 °C</p>
                <p style={{ color: "#E2E0DE" }}>Partly Cloudy</p>
              </Grid>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
                />
                <p style={{ color: "#E2E0DE" }}>Saturday</p>
                <p style={{ color: "#E2E0DE" }}>25.3 °C</p>
                <p style={{ color: "#E2E0DE" }}>Rainy</p>
              </Grid>

            </Grid>

          </Box> : ""}
        {chart ?
          <Box sx={{ display: 'flex', flexDirection: 'column', marginLeft: "60px", width: 1100, height: 350, backgroundColor: '#190D87', borderRadius: "20px" }}>
            <Grid xs container direction="row" spacing={2} rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
              <CardMedia sx={{ height: 40, width: 70, marginLeft: "50px", marginTop: "40px" }}
                component="img"
                image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
              />
              <Grid item xs={5} sx={{ marginLeft: '20px' }}>

                <h1 style={{ color: "#E2E0DE" }}>78.4 °F Sunday</h1>
                <p style={{ color: "#E2E0DE" }}>Light Rain shower</p>
              </Grid>
              <Grid xs container direction="row" style={{ marginLeft: "40px" }}>
                <Grid xs container columnSpacing={{ xs: 1 }}><p style={{ width: "40px", color: "#E2E0DE", marginBotton:8 }}>Wind 11.2 mph</p></Grid>
                <Grid xs container columnSpacing={{ xs: 1 }}><p style={{ width: "40px", color: "#E2E0DE", marginBottom:8}}>Precip 0.00mm</p></Grid>
                <Grid xs container columnSpacing={{ xs: 1 }}><p style={{ width: "40px", color: "#E2E0DE", marginBotton:8 }}>Pressure 29.6 in</p></Grid>
              </Grid>
            </Grid>
            <Grid xs container direction="row" style={{ marginLeft: "30px" }} spacing={2} rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
                />
                <p style={{ color: "#E2E0DE" }}>Monday</p>
                  <p style={{ color: "#E2E0DE" }}>
                  83.9 °F</p>
                  <p style={{ color: "#E2E0DE" }}>
                  Partly Cloudy</p>
              </Grid>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
                />
                <p style={{ color: "#E2E0DE" }}>Tuesday</p>
                <p style={{ color: "#E2E0DE" }}>85.3 °F</p>
                <p style={{ color: "#E2E0DE" }}>Cloudy</p>
              </Grid>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 50, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
                />
                <p style={{ color: "#E2E0DE" }}>Wednesday</p>
                <p style={{ color: "#E2E0DE" }}>85.0 °F</p>
                <p style={{ color: "#E2E0DE" }}>Sunshine </p>
              </Grid>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
                />
                <p style={{ color: "#E2E0DE" }}>Thursday</p>
                <p style={{ color: "#E2E0DE" }}>76.9 °F </p>
                <p style={{ color: "#E2E0DE" }}>Rainy</p>
              </Grid>

              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
                />
                <p style={{ color: "#E2E0DE" }}>Friday</p>
                <p style={{ color: "#E2E0DE" }}>79.0 °F</p>
                <p style={{ color: "#E2E0DE" }}>Partly Cloudy</p>
              </Grid>
              <Grid item xs={1.5} sx={{ marginLeft: '20px' }}>
                <CardMedia sx={{ height: 40, width: 50 }}
                  component="img"
                  image="http://cdn.weatherapi.com/weather/64x64/day/116.png"
                />
                <p style={{ color: "#E2E0DE" }}>Saturday</p>
                <p style={{ color: "#E2E0DE" }}>80.0 °F</p>
                <p style={{ color: "#E2E0DE" }}>Rainy</p>
              </Grid>
            </Grid>
          </Box> : ""}
      </Box>
    </>
  );
}
