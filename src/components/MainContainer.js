import * as React from 'react';
import { styled } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Search from './Search';
import Card from './Card';
import TableData from './TableData';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

export default function MainContainer() {
  const [user, setUser] = React.useState(false)
  return (
    <Box sx={{ width: '100%', marginLeft: "40px", marginTop: "20px" }}>
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>

        <Grid item xs={11}>
          <Search />
        </Grid>
        <Grid item xs={11}>
          <Card />
        </Grid>
        <Grid item xs={11}>
          <h3 style={{ marginLeft: "30px" }}>Today's weather in Hyderabad</h3>
          <Box sx={{ display: 'flex', flexDirection: 'column', marginLeft: "60px", width: 1100, height: 100, backgroundColor: 'white', border: 'solid', borderBlockColor: 'black' }}>
            <Grid xs container direction="row" spacing={2} rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
              <Grid item xs={2} sx={{ marginLeft: '20px' }}>
                <p style={{ color: "Black" }}>Sunrise 06:17am</p>
                <p style={{ color: "Black" }}>Sunset 07:11pm</p>
              </Grid>
              <Grid item xs={2} sx={{ marginLeft: '20px' }}>
                <p style={{ color: "Black" }}>Moonrise 03:49pm</p>
                <p style={{ color: "Black" }}>Moonset 01:50am</p>
              </Grid>
              <Grid item xs={1} sx={{ marginLeft: '20px' }}>
                <p style={{ color: "Black" }}>Max</p>
                <p style={{ color: "Black" }}>30.7 C</p>
              </Grid>
              <Grid item xs={1} sx={{ marginLeft: '20px' }}>
                <p style={{ color: "Black" }}>Min</p>
                <p style={{ color: "Black" }}>24 C</p>
              </Grid>
              <Grid item xs={1} sx={{ marginLeft: '20px' }}>
                <p style={{ color: "Black" }}>Avg</p>
                <p style={{ color: "Black" }}>06:08 pm</p>
              </Grid>
              <Grid item xs={1} sx={{ marginLeft: '20px' }}>
                <p style={{ color: "Black" }}>precip</p>
                <p style={{ color: "Black" }}>9.19mm</p>
              </Grid>
              <Grid item xs={1} sx={{ marginLeft: '20px' }}>
                <p style={{ color: "Black" }}>Max.wind</p>
                <p style={{ color: "Black" }}>13.0kph</p>
              </Grid>
            </Grid>
          </Box>
        </Grid>
        <Grid item xs={11}>
          <TableData />
        </Grid>
      </Grid>
    </Box>
  );
}
